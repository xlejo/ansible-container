# Ansible Container

## Requirements:

- podman or docker

## Steps

1. Edit your .bashrc or .zshrc and pass this:

```bash
function ansible_container(){
        podman run --rm -it \
                --security-opt label=disable \
                -v $(dirname $SSH_AUTH_SOCK):$(dirname $SSH_AUTH_SOCK):ro \
                -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK \
                -v $(pwd):/workdir \    
                -v $HOME/.ssh:/root/.ssh:ro \
                -v /tmp:/tmp \
                -w /workdir \    
                gitlab.com/xlejo/ansible-container:latest-alpine \
                $@
}

alias ansible='ansible_container ansible'
alias ansible-connection='ansible_container ansible-connection'                                                                                                             
alias ansible-doc='ansible_container ansible-doc'                                                                                                             
alias ansible-inventory='ansible_container ansible-inventory'                                                                                                              
alias ansible-pull='ansible_container ansible-pull'                                                                                                             
alias ansible-vault='ansible_container ansible-vault'                                                                                                             
alias ansible-config='ansible_container ansible-config'                                                                                                             
alias ansible-console='ansible_container ansible-console'                                                                                                             
alias ansible-galaxy='ansible_container ansible-galaxy'                                                                                                             
alias ansible-playbook='ansible_container ansible-playbook'
alias ansible-test='ansible_container ansible-test'
```

If you use Docker just change `podman run` by `docker run`.

## Usage

Just run like if you already installed in your host machine :)

```bash
ansible -m ansible.builtin.setup localhost
```

